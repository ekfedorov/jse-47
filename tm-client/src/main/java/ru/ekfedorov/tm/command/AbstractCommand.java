package ru.ekfedorov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.endpoint.EndpointLocator;
import ru.ekfedorov.tm.api.service.ServiceLocator;
import ru.ekfedorov.tm.bootstrap.Bootstrap;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public abstract class AbstractCommand {

    @Nullable
    protected Bootstrap bootstrap;

    @Nullable
    protected EndpointLocator endpointLocator;

    @Nullable
    protected ServiceLocator serviceLocator;

    @Nullable
    public abstract String commandArg();

    @Nullable
    public abstract String commandDescription();

    @Nullable
    public abstract String commandName();

    public abstract void execute();

    public void setBootstrap(@Nullable final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void setEndpointLocator(@NotNull final EndpointLocator endpointLocator) {
        this.endpointLocator = endpointLocator;
    }

    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    public String toString() {
        String result = "";
        if (!isEmpty(commandName())) result += commandName();
        if (!isEmpty(commandArg())) result += " [" + commandArg() + "]";
        if (!isEmpty(commandDescription())) result += " - " + commandDescription();
        return result;
    }

}
