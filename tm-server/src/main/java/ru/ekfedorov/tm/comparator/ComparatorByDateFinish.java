package ru.ekfedorov.tm.comparator;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.entity.IHasDateFinish;

import java.util.Comparator;

@NoArgsConstructor
public final class ComparatorByDateFinish implements Comparator<IHasDateFinish> {

    @NotNull
    private static final ComparatorByDateFinish INSTANCE = new ComparatorByDateFinish();

    @NotNull
    public static ComparatorByDateFinish getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(
            @Nullable final IHasDateFinish o1, @Nullable final IHasDateFinish o2
    ) {
        if (o1 == null || o2 == null) return 0;
        return o1.getDateFinish().compareTo(o2.getDateFinish());
    }

}
