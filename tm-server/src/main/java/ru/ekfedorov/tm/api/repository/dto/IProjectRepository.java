package ru.ekfedorov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.repository.IRepository;
import ru.ekfedorov.tm.dto.Project;

import java.util.List;
import java.util.Optional;

public interface IProjectRepository extends IRepository<Project> {

    void clear();

    void clearByUserId(@NotNull String userId);

    @NotNull
    List<Project> findAll();

    @NotNull
    List<Project> findAllByUserId(@Nullable String userId);

    @NotNull
    Optional<Project> findOneById(@Nullable String id);

    @NotNull
    Optional<Project> findOneByIdAndUserId(
            @Nullable String userId, @NotNull String id
    );

    @NotNull
    Optional<Project> findOneByIndex(
            @Nullable String userId, @NotNull Integer index
    );

    Optional<Project> findOneByName(
            @Nullable String userId, @NotNull String name
    );

    void removeOneById(@Nullable String id);

    void removeOneByIdAndUserId(@Nullable String userId, @NotNull String id);

    void removeOneByName(
            @Nullable String userId, @NotNull String name
    );

}
