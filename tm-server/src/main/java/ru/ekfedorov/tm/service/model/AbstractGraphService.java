package ru.ekfedorov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.ekfedorov.tm.api.service.IConnectionService;
import ru.ekfedorov.tm.api.service.model.IGraphService;
import ru.ekfedorov.tm.model.AbstractGraphEntity;


public abstract class AbstractGraphService<E extends AbstractGraphEntity> implements IGraphService<E> {

    @NotNull
    public final IConnectionService connectionService;

    public AbstractGraphService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
