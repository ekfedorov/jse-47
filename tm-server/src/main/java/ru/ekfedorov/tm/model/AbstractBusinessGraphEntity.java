package ru.ekfedorov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractBusinessGraphEntity extends AbstractGraphEntity {

    @Column
    @NotNull
    protected Date created = new Date();

    @Nullable
    @Column(name = "date_finish")
    protected Date dateFinish;

    @Nullable
    @Column(name = "date_start")
    protected Date dateStart;

    @Column
    @Nullable
    protected String description = "";

    @Column
    @Nullable
    protected String name = "";

    @NotNull
    @Enumerated(EnumType.STRING)
    protected Status status = Status.NOT_STARTED;

    @Nullable
    @ManyToOne
    private UserGraph user;

}

